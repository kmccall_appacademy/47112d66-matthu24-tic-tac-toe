class ComputerPlayer
  attr_accessor :name,:board,:mark
  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    #get all previous computer moves from board
    previous_computer_moves = []
    #self.board.grid.each_with_index do |row,row_index|
    display(@board).grid.each_with_index do |row,row_index|
      row.each_with_index do |col,col_index|
        if col == :O
          previous_computer_moves << [row_index,col_index]
        end
      end
    end

    #get all 2 position combinations of all the moves the computer has made thus far
    all_combos = []
    previous_computer_moves.each_with_index do |arr,index1|
      (index1+1..previous_computer_moves.length-1).each do |index2|
        all_combos << [previous_computer_moves[index1],previous_computer_moves[index2]]
      end
    end

    #make array of arrays for winning positions
    winners = [
      [[0,0],[0,1],[0,2]],[[1,0],[1,1],[2,1]],[[2,0],[2,1],[2,2]],
      [[0,0],[1,0],[2,0]],[[0,1],[1,1],[2,1]],[[0,2],[1,2],[2,2]],
      [[0,0],[1,1],[2,2]],[[0,2],[1,1],[2,0]]
    ]

    winners.each do |winning_position|
      all_combos.each do |combo|
        #winning_move returns an array of arrays
        winning_move = winning_position.select{|position| combo.include?(position) == false}
        if winning_move.length == 1 && display(@board).empty?(winning_move[0])
          return winning_move[0]
        end
      end
    end

    rand_move = [rand(3),rand(3)]
    while display(@board).empty?(rand_move) == false
      rand_move = [rand(3),rand(3)]
    end
    return rand_move
  end

end
