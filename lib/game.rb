require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player

  def initialize(player_one,player_two)
    #pass in human_player and computer_player instances to game
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  #loop through play_turn until there is a winner
  #need to use board.over? instead
  def play
    while @board.over? == false
      self.play_turn
    end
    if @board.winner != nil
      puts "#{@board.winner} wins"
    else
      puts "draw"
    end 
    print @board.grid
  end

  def play_turn
    #place_mark accepts (position,mark)
    board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
    #need to pass the new board to @current_player
    @current_player.display(@board)
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

end
