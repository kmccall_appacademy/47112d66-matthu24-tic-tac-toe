class Board
  attr_accessor :grid
  def initialize(grid =[[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]] )
    @grid = grid
  end

  def place_mark(position,mark)
    row = position[0]
    column = position[1]
    @grid[row][column] = mark
  end

  def empty?(position)
    row = position[0]
    column = position[1]
    return true if @grid[row][column] == nil
    return false
  end

  def winner
    #columns
    if @grid[0][2] == :X && @grid[1][2] == :X && @grid[2][2] == :X
      return :X
    elsif @grid[0][1] == :X && @grid[1][1] == :X && @grid[2][1] == :X
      return :X
    elsif @grid[0][0] == :X && @grid[1][0] == :X && @grid[2][0] == :X
      return :X
    elsif @grid[0][2] == :O && @grid[1][2] == :O && @grid[2][2] == :O
      return :O
    elsif @grid[0][1] == :O && @grid[1][1] == :O && @grid[2][1] == :O
      return :O
    elsif @grid[0][0] == :O && @grid[1][0] == :O && @grid[2][0] == :O
      return :O
    #rows
    elsif @grid[0][0] == :X && @grid[0][1] == :X && @grid[0][2] == :X
      return :X
    elsif @grid[1][0] == :X && @grid[1][1] == :X && @grid[1][2] == :X
      return :X
    elsif @grid[2][0] == :X && @grid[2][1] == :X && @grid[2][2] == :X
      return :X
    elsif @grid[0][0] == :O && @grid[0][1] == :O && @grid[0][2] == :O
      return :O
    elsif @grid[1][0] == :O && @grid[1][1] == :O && @grid[1][2] == :O
      return :O
    elsif @grid[2][0] == :O && @grid[2][1] == :O && @grid[2][2] == :O
      return :O
    #diagonals
    elsif @grid[0][2] == :O && @grid[1][1] == :O && @grid[2][0] == :O
      return :O
    elsif @grid[0][0] == :O && @grid[1][1] == :O && @grid[2][2] == :O
      return :O
    elsif @grid[0][2] == :X && @grid[1][1] == :X && @grid[2][0] == :X
      return :X
    elsif @grid[0][0] == :X && @grid[1][1] == :X && @grid[2][2] == :X
      return :X
    else
      return nil
    end
  end

  def cats_game?
    cats_game = true
    @grid.each do |row|
      row.each do |square|
        cats_game = false if square == nil
      end
    end
    return true if cats_game == true
    return false
  end

  def over?
    #it's over if there's a winner or if the board is full
    return true if self.winner != nil || cats_game?
    return false
  end

end
