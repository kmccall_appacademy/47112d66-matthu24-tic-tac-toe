class HumanPlayer

  attr_accessor :name, :mark
  def initialize(name)
    @name = name
    @mark = :X
  end

  def display(board)
    print board.grid
  end

  def get_move
    puts "\nwhere"
    move = gets.chomp
    position = [move[1].to_i,move[3].to_i]
    return position
  end
end
